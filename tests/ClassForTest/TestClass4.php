<?php

namespace Tests\ClassForTest;

use App\ValuesObject\Money;
use DateTime;

class TestClass4
{
    private $all;

    public function getVal1()
    {
        return $this->all['val1'];
    }

    public function setVal1($val): void
    {
        $this->all['val1'] = $val;
    }

    public function getVal2(): int
    {
        return $this->all['val2'];
    }

    public function setVal2(int $val): void
    {
        $this->all['val2'] = $val;
    }

    public function getVal3(): float
    {
        return $this->all['val3'];
    }

    public function setVal3(float $val): void
    {
        $this->all['val3'] = $val;
    }

    public function getVal4(): string
    {
        return $this->all['val4'];
    }

    public function setVal4(string $val): void
    {
        $this->all['val4'] = $val;
    }

    public function getVal5(): DateTime
    {
        return $this->all['val5'];
    }

    public function setVal5(DateTime $val): void
    {
        $this->all['val5'] = $val;
    }

    public function getVal6(): Money
    {
        return $this->all['val6'];
    }

    public function setVal6(Money $val): void
    {
        $this->all['val6'] = $val;
    }
}
