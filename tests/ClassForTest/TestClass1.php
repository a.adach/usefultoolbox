<?php

namespace Tests\ClassForTest;

class TestClass1
{
    public function getVal1()
    {
        return 1;
    }

    public function getVal2()
    {
        return 1.5;
    }

    public function getVal3(): bool
    {
        return false;
    }
}
