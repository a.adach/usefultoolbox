<?php

declare(strict_types=1);

namespace Tests\AdachSoft\Toolbox\Transformer;

use AdachSoft\Toolbox\Converter\Model\KeyConverterInterface;
use AdachSoft\Toolbox\Converter\Model\TypeConverterInterface;
use AdachSoft\Toolbox\Service\SettersRetriever;
use AdachSoft\Toolbox\Transformer\Model\ObjectTransformerInterface;
use AdachSoft\Toolbox\Transformer\Spl2DtoObjectTransformer;
use PHPUnit\Framework\TestCase;

class Spl2DtoObjectTransformerTest extends TestCase
{
    public function testInstanceOf(): void
    {
        $settersRetriever = $this->createMock(SettersRetriever::class);
        $keyConverter = $this->createMock(KeyConverterInterface::class);
        $typeConverter = $this->createMock(TypeConverterInterface::class);

        $objectTransformer = new Spl2DtoObjectTransformer($settersRetriever, $keyConverter, $typeConverter);

        $this->assertInstanceOf(ObjectTransformerInterface::class, $objectTransformer);
    }

    public function testTransform(): void
    {
        $fromObject = $this->getObject();
        $toObject = $this->getObjectDTO();

        $settersRetriever = $this->createMock(SettersRetriever::class);
        $keyConverter = $this->createMock(KeyConverterInterface::class);
        $typeConverter = $this->createMock(TypeConverterInterface::class);

        $settersRetriever->method('getAll')->willReturn(['setTestInt' => 'int', 'setTestStr' => 'string', 'setTestArr' => 'array', 'setTestObj' => 'object']);

        $typeConverter->expects($this->at(0))->method('convert')->willReturn($fromObject->testInt);
        $typeConverter->expects($this->at(1))->method('convert')->willReturn($fromObject->testStr);
        $typeConverter->expects($this->at(2))->method('convert')->willReturn($fromObject->testArr);
        $typeConverter->expects($this->at(3))->method('convert')->willReturn($fromObject->testObj);

        $keyConverter->expects($this->at(0))->method('convert')->willReturn('testInt');
        $keyConverter->expects($this->at(1))->method('convert')->willReturn('testStr');
        $keyConverter->expects($this->at(2))->method('convert')->willReturn('testArr');
        $keyConverter->expects($this->at(3))->method('convert')->willReturn('testObj');

        $objectTransformer = new Spl2DtoObjectTransformer($settersRetriever, $keyConverter, $typeConverter);

        $objectTransformer->transform($fromObject, $toObject);

        $this->assertSame($fromObject->testInt, $toObject->getTestInt());
        $this->assertSame($fromObject->testStr, $toObject->getTestStr());
        $this->assertSame($fromObject->testArr, $toObject->getTestArr());
        $this->assertSame($fromObject->testObj, $toObject->getTestObj());
    }

    private function getObject(): object
    {
        $obj = new class {
            public $testInt = 1;

            public $testStr = 'ABC';

            public $testArr = [1, 2, 3];

            public $testObj;
        };

        $obj->testObj = $this->getObject1();

        return $obj;
    }

    private function getObjectDTO(): object
    {
        return new class {
            private $testInt;

            private $testStr;

            private $testArr;

            private $testObj;

            public function setTestInt(int $testInt): void
            {
                $this->testInt = $testInt;
            }

            public function getTestInt(): int
            {
                return $this->testInt;
            }

            public function setTestStr(string $testStr): void
            {
                $this->testStr = $testStr;
            }

            public function getTestStr(): string
            {
                return $this->testStr;
            }

            public function setTestArr(array $testArr): void
            {
                $this->testArr = $testArr;
            }

            public function getTestArr(): array
            {
                return $this->testArr;
            }

            public function setTestObj(object $testObj): void
            {
                $this->testObj = $testObj;
            }

            public function getTestObj(): object
            {
                return $this->testObj;
            }
        };
    }

    private function getObject1(): object
    {
        return new class {
            public $val = 72;

            public $name = 'test';
        };
    }
}
