<?php

declare(strict_types=1);

namespace Tests\AdachSoft\App\Converter;

use AdachSoft\Toolbox\Converter\Model\KeyConverterInterface;
use AdachSoft\Toolbox\Converter\WithoutGetKeyConverter;
use PHPUnit\Framework\TestCase;

class WithoutGetKeyConverterTest extends TestCase
{
    public function testInstanceOf(): void
    {
        $keyConverter = new WithoutGetKeyConverter();
        $this->assertInstanceOf(KeyConverterInterface::class, $keyConverter);
    }

    /**
     * @dataProvider dataConvert
     */
    public function testConvert(string $expectedValue, string $valueIn): void
    {
        $keyConverter = new WithoutGetKeyConverter();
        $this->assertSame($expectedValue, $keyConverter->convert($valueIn));
    }

    public function dataConvert(): array
    {
        return [
            ['name', 'getName'],
            ['testName', 'getTestName'],
            ['testName432', 'testName432'],
            ['abcValueTest', 'AbcValueTest'],
        ];
    }
}
