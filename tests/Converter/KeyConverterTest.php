<?php

declare(strict_types=1);

namespace Tests\AdachSoft\App\Converter;

use AdachSoft\Toolbox\Converter\KeyConverter;
use AdachSoft\Toolbox\Converter\Model\KeyConverterInterface;
use PHPUnit\Framework\TestCase;

class KeyConverterTest extends TestCase
{
    public function testInstanceOf(): void
    {
        $keyConverter = new KeyConverter();
        $this->assertInstanceOf(KeyConverterInterface::class, $keyConverter);
    }

    public function testConvert(): void
    {
        $keyConverter = new KeyConverter();
        $this->assertSame('getName', $keyConverter->convert('getName'));
    }
}
