<?php

declare(strict_types=1);

namespace Tests\AdachSoft\App\Converter;

use AdachSoft\Toolbox\Converter\Model\KeyConverterInterface;
use AdachSoft\Toolbox\Converter\SetterToPropertyKeyConverter;
use PHPUnit\Framework\TestCase;

class SetterToPropertyKeyConverterTest extends TestCase
{
    public function testInstanceOf(): void
    {
        $keyConverter = new SetterToPropertyKeyConverter();
        $this->assertInstanceOf(KeyConverterInterface::class, $keyConverter);
    }

    /**
     * @dataProvider dataConvert
     */
    public function testConvert(string $expectedValue, string $valueIn): void
    {
        $keyConverter = new SetterToPropertyKeyConverter();
        $this->assertSame($expectedValue, $keyConverter->convert($valueIn));
    }

    public function dataConvert(): array
    {
        return [
            ['name', 'setName'],
            ['testName', 'setTestName'],
            ['val123', 'setVal123']
        ];
    }
}
