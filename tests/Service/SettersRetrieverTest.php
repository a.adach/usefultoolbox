<?php

namespace Tests\Unit\App\Service;

use AdachSoft\Toolbox\Service\SettersRetriever;
use PHPUnit\Framework\TestCase;
use Tests\ClassForTest\TestClass4;

class SettersRetrieverTest extends TestCase
{
    public function testGetAll(): void
    {
        $object = new TestClass4();
        $object->setVal1('fdggddffd');
        $object->setVal2(456);

        $settersRetriever = new SettersRetriever();
        $result = $settersRetriever->getAll($object);
        $this->assertIsArray($result);

        $this->assertEquals('mixed', $result['setVal1']);
        $this->assertEquals('int', $result['setVal2']);
        $this->assertEquals('float', $result['setVal3']);
        $this->assertEquals('string', $result['setVal4']);
        $this->assertEquals('DateTime', $result['setVal5']);
        $this->assertEquals('App\ValuesObject\Money', $result['setVal6']);
    }

    public function testGetAllFromClassName(): void
    {
        $settersRetriever = new SettersRetriever();
        $result = $settersRetriever->getAllFromClassName(TestClass4::class);
        $this->assertIsArray($result);

        $this->assertEquals('mixed', $result['setVal1']);
        $this->assertEquals('int', $result['setVal2']);
        $this->assertEquals('float', $result['setVal3']);
        $this->assertEquals('string', $result['setVal4']);
        $this->assertEquals('DateTime', $result['setVal5']);
        $this->assertEquals('App\ValuesObject\Money', $result['setVal6']);
    }
}
