<?php

namespace Tests\AdachSoft\App\Service;

use AdachSoft\Toolbox\Service\GettersRetriever;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Tests\ClassForTest\TestClass1;
use Tests\ClassForTest\TestClass2;
use Tests\ClassForTest\TestClass3;

class GettersRetrieverTest extends TestCase
{
    /**
     * @dataProvider dataGetAll
     */
    public function testGetAll(object $object, array $methods): void
    {
        $gettersRetriever = new GettersRetriever();
        $result = $gettersRetriever->getAll($object);
        $this->assertIsArray($result);
        $this->assertCount(count($methods), $result);
        $this->assertArraySubset($methods, $result);
    }

    /**
     * @dataProvider dataGetAll
     */
    public function testGetAllFromClassName(object $object, array $methods): void
    {
        $gettersRetriever = new GettersRetriever();
        $result = $gettersRetriever->getAllFromClassName(get_class($object));
        $this->assertIsArray($result);
        $this->assertCount(count($methods), $result);
        $this->assertArraySubset($methods, $result);
    }

    /**
     * @dataProvider dataShouldThrowException
     */
    public function testShouldThrowException(): void
    {
        $this->expectException(RuntimeException::class);

        $gettersRetriever = new GettersRetriever();
        $gettersRetriever->getAllFromClassName('11344');
    }

    public function dataGetAll(): array
    {
        return [
            [$this->createObject(), ['getVal']],
            [new TestClass1(), ['getVal1', 'getVal2', 'getVal3']],
            [new TestClass2(), ['getVal4', 'getVal1', 'getVal2', 'getVal3']],
            [new TestClass3(), ['getVal1', 'getVal2']],
        ];
    }

    public function dataShouldThrowException(): array
    {
        return [
            ['gfgdf'],
            ['1234'],
            ['1234'],
        ];
    }

    private function createObject(): object
    {
        return new class() {
            public function getVal(): int
            {
                return 123;
            }
        };
    }

    private function assertArraySubset(array $expected, array $arrayIn): void
    {
        foreach($expected as $value) {
            $this->assertContains($value, $arrayIn);
        }
    }
}
