<?php

declare(strict_types=1);

namespace AdachSoft\Toolbox\Json;

use AdachSoft\Toolbox\Json\Exception\JsonException;
use JsonException as BaseJsonException;

class Json
{
    /**
     * @param mixed $value
     * @return string
     * @throws JsonException
     */
    public static function encode($value): string
    {
        $result = json_encode($value);
        if (!is_string($result)) {
            throw new JsonException('Failed to encode value to JSON');
        }

        return $result;
    }

    /**
     * @param string $jsonString
     * @return array
     * @throws JsonException
     */
    public static function decode(string $jsonString): array
    {
        try {
            return json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR);
        } catch (BaseJsonException $e) {
            throw new JsonException($e->getMessage(), $e->getCode(), $e);
        }
    }
}
