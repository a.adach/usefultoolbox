<?php

namespace AdachSoft\Toolbox\Service;

use ReflectionClass;
use ReflectionException;
use RuntimeException;

class SettersRetriever
{
    /**
     * @return string[]
     */
    public function getAll(object $fromObject): array
    {
        return $this->getAllFromClassName(get_class($fromObject));
    }

    /**
     * @return string[]
     */
    public function getAllFromClassName(string $className): array
    {
        try{
            return $this->getSetters($className);
        } catch (ReflectionException $e) {
            throw new RuntimeException($e->getMessage());
        }
    }

    private function getSetters(string $className): array
    {
        $setters = [];
        $class = new ReflectionClass($className);
        foreach ($class->getMethods() as $method) {
            if ($this->isGoodMethod($method)) {
                $parameters = $method->getParameters();
                if (count($parameters) !== 1) {
                    throw new RuntimeException('Only one parameter allowed');
                }
                $type = $parameters[0]->getType();
                $typeName = null === $type ? 'mixed' : $type->getName();
                $setters[$method->getName()] = $typeName;
            }
        }

        return $setters;
    }

    private function isGoodMethod($method): bool
    {
        return $method->isPublic() && !$method->isStatic() && preg_match("|^set|i", $method->getName());
    }
}
