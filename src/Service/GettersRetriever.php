<?php

namespace AdachSoft\Toolbox\Service;

use ReflectionClass;
use ReflectionException;
use RuntimeException;

class GettersRetriever
{
    /**
     * @return string[]
     */
    public function getAll(object $fromObject): array
    {
        return $this->getAllFromClassName(get_class($fromObject));
    }

    /**
     * @return string[]
     */
    public function getAllFromClassName(string $className): array
    {
        try{
            $getters = [];
            $class = new ReflectionClass($className);
            foreach ($class->getMethods() as $method) {
                if ($this->isGoodMethod($method)) {
                    $getters[] = $method->getName();
                }
            }
    
            return $getters;
        }catch(ReflectionException $e) {
            throw new RuntimeException($e->getMessage());
        }
    }

    private function isGoodMethod($method): bool
    {
        return $method->isPublic() && !$method->isStatic() && preg_match("|^get|i", $method->getName());
    }
}
