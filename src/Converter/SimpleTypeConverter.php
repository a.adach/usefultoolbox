<?php

declare(strict_types=1);

namespace AdachSoft\Toolbox\Converter;

use AdachSoft\Toolbox\Converter\Model\TypeConverterInterface;

class SimpleTypeConverter implements TypeConverterInterface
{
    public function convert($valueIn, string $returnType)
    {
        if ('string' === $returnType) {
            return (string) $valueIn;
        }

        return $valueIn;
    }
}
