<?php

declare(strict_types=1);

namespace AdachSoft\Toolbox\Converter\Model;

interface KeyConverterInterface
{
    public function convert(string $valueIn): string;
}
