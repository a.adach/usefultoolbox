<?php

declare(strict_types=1);

namespace AdachSoft\Toolbox\Converter\Model;

interface TypeConverterInterface
{
    /**
     * @param mixed $valueIn
     * @param string $returnType
     * @return mixed
     */
    public function convert($valueIn, string $returnType);
}
