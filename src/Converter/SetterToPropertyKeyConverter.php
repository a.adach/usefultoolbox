<?php

declare(strict_types=1);

namespace AdachSoft\Toolbox\Converter;

use AdachSoft\Toolbox\Converter\Model\KeyConverterInterface;

class SetterToPropertyKeyConverter implements KeyConverterInterface
{
    public function convert(string $valueIn): string
    {
        if (false !== strpos($valueIn, 'set')) {
            $valueIn = (substr($valueIn, 3));
        }

        return lcfirst($valueIn);
    }
}
