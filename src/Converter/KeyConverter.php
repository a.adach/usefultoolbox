<?php

declare(strict_types=1);

namespace AdachSoft\Toolbox\Converter;

use AdachSoft\Toolbox\Converter\Model\KeyConverterInterface;

class KeyConverter implements KeyConverterInterface
{
    public function convert(string $valueIn): string
    {
        return $valueIn;
    }
}
