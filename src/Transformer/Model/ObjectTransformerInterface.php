<?php

declare(strict_types=1);

namespace AdachSoft\Toolbox\Transformer\Model;

interface ObjectTransformerInterface
{
    public function transform(object $fromObject, object $toObject): void;
}
