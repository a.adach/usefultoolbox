<?php

declare(strict_types=1);

namespace AdachSoft\Toolbox\Transformer;

use AdachSoft\Toolbox\Converter\Model\KeyConverterInterface;
use AdachSoft\Toolbox\Converter\Model\TypeConverterInterface;
use AdachSoft\Toolbox\Service\SettersRetriever;
use AdachSoft\Toolbox\Transformer\Model\ObjectTransformerInterface;

class Spl2DtoObjectTransformer implements ObjectTransformerInterface
{
    /**
     * @var SettersRetriever
     */
    private $settersRetriever;
    
    /**
     * @var KeyConverterInterface
     */
    private $keyConverter;

    /**
     * @var TypeConverterInterface
     */
    private $typeConverter;

    public function __construct(
        SettersRetriever $settersRetriever,
        KeyConverterInterface $keyConverter,
        TypeConverterInterface $typeConverter
    ) {
        $this->settersRetriever = $settersRetriever;
        $this->keyConverter = $keyConverter;
        $this->typeConverter = $typeConverter;
    }

    public function transform(object $fromObject, object $toObject): void
    {
        $setters = $this->settersRetriever->getAll($toObject);

        foreach ($setters as $methodName => $methodType) {
            $property = $this->keyConverter->convert($methodName);
            $value = $this->typeConverter->convert($fromObject->$property, $methodType);
            $toObject->$methodName($value);
        }
    }
}
