<?php

namespace AdachSoft\Toolbox\Collection;

use RuntimeException;

abstract class AbstractCollection implements CollectionInterface
{
    /**
     * @var int
     */
    protected $currentKeyIndex = 0;

    /**
     * @var int
     */
    protected $position = 0;

    /**
     * @var array
     */
    protected $container = [];

    /**
     * @var array
     */
    protected $keys = [];

    public function __construct($value=[])   
    {
        $this->position = 0;
        if (is_array($value)) {
            $this->addFromArray($value);
        } else {
            $this->add(null, $value);
        }
    }

    public static function createCollection($value = []): CollectionInterface
    {
        $className = get_called_class();

        return new $className($value);
    }

    /**
     * {@inheritDoc} - CollectionInterface
     */
    public function filter(callable $callback): CollectionInterface
    {
        $filteredCollection = static::createCollection();
        foreach($this as $item) {
            if ($callback($item)) {
                $filteredCollection[] = $item;
            }
        }
        
        return $filteredCollection;
    }

    /**
     * {@inheritDoc} - CollectionInterface
     */
    public function indexExists(int $index): bool
    {
        return isset($this->keys[$index]);
    }

    /**
     * {@inheritDoc} - CollectionInterface
     */
    public function indexOf(int $index)
    {
        if (!isset($this->keys[$index])) {
            throw new RuntimeException("Undefined offset: {$index}");
        }
        $key = $this->keys[$index];

        return $this->container[$key];
    }

    /**
     * {@inheritDoc} - CollectionInterface
     */
    public function addFromArray(array $data): void
    {
        foreach($data as $key => $item) {
            $this->add($key, $item);
        }
    }

    /**
     * {@inheritDoc} - CollectionInterface
     */
    public function addFromCollection( $collection): void
    {
        foreach($collection as $key => $item) {
            $this->add($key, $item);
        }
    }

    /**
     * {@inheritDoc} - CollectionInterface
     */
    public function add($key, $value): void
    {
        $this->assertType($value);
        
        if (null === $key) {
            while (isset($this->container[$this->currentKeyIndex])) {
                $this->currentKeyIndex++;
            }

            $this->container[$this->currentKeyIndex] = $value;
            $this->keys[] = $this->currentKeyIndex;

            $this->currentKeyIndex++;
        } else {
            $this->keys[] = $key;
            $this->container[$key] = $value;
        }
    }

    /**
     * {@inheritDoc} - CollectionInterface
     */
    public function isEmpty(): bool
    {
        return empty($this->container);
    }

    /**
     * {@inheritDoc} - CollectionInterface
     */
    public function first()
    {
        if ($this->isEmpty()) {
            throw new RuntimeException("Collection is empty");
        }
        $key = $this->keys[0];
        
        return $this->container[$key];
    }

    /**
     * {@inheritDoc} - CollectionInterface
     */
    public function removeIndex(int $index): void
    {
        if (!isset($this->keys[$index])) {
            throw new RuntimeException("Undefined offset: {$index}");
        }
        $key = $this->keys[$index];

        unset($this->container[$key]);
        unset($this->keys[$index]);

        $this->keys = array_values($this->keys);
    }

    /**
     * {@inheritDoc} - CollectionInterface
     */
    public function slice(int $offset, ?int $length = null): CollectionInterface
    {
        $slicedCollection = static::createCollection(
            array_slice(iterator_to_array($this), $offset, $length)
        );

        return $slicedCollection;
    }

    /**
     * {@inheritDoc} - CollectionInterface
     */
    public function toArray(): array
    {
        return iterator_to_array($this);
    }

    /**
     * {@inheritDoc} - Iterator
     */
    public function rewind(): void
    {
        $this->position = 0;
    }

    /**
     * {@inheritDoc} - Iterator
     */
    public function current() 
    {
        $key = $this->keys[$this->position];

        return $this->container[$key];
    }

    /**
     * {@inheritDoc} - Iterator
     */
    public function key() 
    {
        return $this->keys[$this->position];
    }

    /**
     * {@inheritDoc} - Iterator
     */
    public function next(): void
    {
        ++$this->position;
    }

    /**
     * {@inheritDoc} - Iterator
     */
    public function valid(): bool
    {
        if (!isset($this->keys[$this->position])) {
            return false;
        }
        $key = $this->keys[$this->position];

        return isset($this->container[$key]);
    }

    /**
     * {@inheritDoc} - ArrayAccess
     */
    public function offsetSet($offset, $value): void
    {
        $this->add($offset, $value);
    }

    /**
     * {@inheritDoc} - ArrayAccess
     */
    public function offsetExists($offset): bool
    {
        return in_array($offset, $this->keys);
    }

    /**
     * {@inheritDoc} - ArrayAccess
     */
    public function offsetUnset($offset): void 
    {
        $key = array_search($offset, $this->keys);

        unset($this->keys[$key]);
        unset($this->container[$offset]);

        $this->keys = array_values($this->keys);
    }

    /**
     * {@inheritDoc} - ArrayAccess
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * {@inheritDoc} - Countable
     */
    public function count(): int
    {
        return count($this->container);
    }

    abstract protected function assertType($value): void;
}
