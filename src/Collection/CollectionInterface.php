<?php

namespace AdachSoft\Toolbox\Collection;

use ArrayAccess;
use Countable;
use Iterator;

interface CollectionInterface extends Iterator, ArrayAccess, Countable
{
    public static function createCollection(): CollectionInterface;

    public function addFromCollection(CollectionInterface $collection): void;

    public function addFromArray(array $data): void;

    public function add($key, $value): void;

    public function isEmpty(): bool;

    public function indexExists(int $index): bool;

    /**
     * @return mixed
     */
    public function indexOf(int $index);

    /**
     * @return mixed
     */
    public function first();

    public function removeIndex(int $index): void;

    public function filter(callable $callback): CollectionInterface;

    public function slice(int $offset, ?int $length = null): CollectionInterface;

    public function toArray(): array;
}
